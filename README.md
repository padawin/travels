# Travels

The code of my website where I display the pictures of my travels...

## To generate the DB and thumbnails:

```
./bin/generate.py storage/ data/travels.json images/
```
